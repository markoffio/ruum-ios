//
//  LoginView.swift
//  Ruum
//
//  Created by Marco Margarucci on 11/04/21.
//

import SwiftUI

struct LoginView: View {
    // State object login view model
    @StateObject private var loginViewModel = LoginViewModel()
    
    // Forgot password
    fileprivate func forgotPassword() -> some View {
        HStack {
            Spacer()
            NavigationLink(destination: PasswordResetView()) {
                Text("forgot password?")
                    .fontWeight(.semibold)
                    .foregroundColor(Color.text)
            }
        }
    }
    
    // Register
    fileprivate func register() -> some View {
        return HStack() {
            Text("Don\'t have an account?")
                .fontWeight(.semibold)
                .foregroundColor(Color.text)
            NavigationLink(destination: RegisterProfileView()) {
                Text("Register!")
                    .fontWeight(.semibold)
                    .foregroundColor(Color.electricBlue)
            }
        }
    }
    
    // Login
    fileprivate func login() -> some View {
        NavigationLink(destination: DashboardView(), isActive: $loginViewModel.loginSuccess)
        {
            Button(action: {
                    hideKeyboard()
                    loginViewModel.loginUser()
            }) {
                Text("LOGIN")
                    .bold()
                    .textStyle(ButtonStyle())
                    .padding(.top, 20)
                    .padding(.horizontal, -8)
            }
        }
    }
    
    var body: some View {
        ZStack {
            NavigationView {
                VStack {
                    ScreenTitle("Hi! \nPlease login ;-)")
                    VStack(spacing: 20) {
                        // Email field
                        EmailField(email: $loginViewModel.email)
                            .padding(.horizontal, 20)
                        // Password secure field
                        PasswordField(password: $loginViewModel.password)
                            .padding(.horizontal, 20)
                            .padding(.top, 16)
                        // Forgot password
                        forgotPassword()
                            .padding(.top, 10)
                            .padding(.horizontal, 30)
                        // Login button
                        login()
                        .padding(.horizontal, 10)
                        Spacer()
                        // Register button
                        register()
                            .padding(.bottom, 20)
                    }
                }
                .alert(isPresented: $loginViewModel.errorOccurred) {
                    Alert(title: Text("Error"), message: Text(loginViewModel.loginError))
                }
            }
            .navigationBarHidden(true)
            if loginViewModel.loading {
                LogginInView()
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
