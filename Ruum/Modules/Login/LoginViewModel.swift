//
//  LoginViewModel.swift
//  Ruum
//
//  Created by Marco Margarucci on 12/04/21.
//

import Foundation

class LoginViewModel: ObservableObject {
    // Email address
    @Published var email: String = ""
    // Password
    @Published var password: String = ""
    // Login success
    @Published var loginSuccess: Bool = false
    // Error occurred
    @Published var errorOccurred: Bool = false
    // Login error
    @Published var loginError: String = ""
    // Loading
    @Published var loading: Bool = false
    
    private let authService = AuthService()
    
    // Login user
    func loginUser() {
        loading = true
        authService.loginUser(withEmail: email, andPassword: password) { [self] (error) in
            if error == nil {
                loading = false
                loginSuccess = true
            } else {
                errorOccurred = true
                loading = false
                loginError = error!.localizedDescription
            }
        }
    }
}
