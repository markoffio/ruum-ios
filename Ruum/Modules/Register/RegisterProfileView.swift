//
//  RegisterProfileView.swift
//  Ruum
//
//  Created by Marco Margarucci on 11/04/21.
//

import SwiftUI

struct RegisterProfileView: View {
    @Environment(\.presentationMode) var presentation
    // State object register view model
    @StateObject private var registerViewModel = RegisterViewModel()
    
    // Register
    fileprivate func register() -> some View {
        NavigationLink(destination: LoginView(), isActive: $registerViewModel.registrationSuccess)
        {
            Button(action: { registerViewModel.register() }) {
                Text("REGISTER")
                    .bold()
                    .textStyle(ButtonStyle())
                    .padding(.top, 20)
                    .padding(.horizontal, -8)
            }
        }
    }
    
    // Display alert
    private func displayAlert() -> Alert {
        Alert(title: Text("Errore"), message: Text(registerViewModel.registerError), dismissButton: .default(Text("OK"), action: {
            //presentationMode.wrappedValue.dismiss()
        }))
    }
    
    var body: some View {
        ZStack {
            ScrollView {
                ScreenTitle("Create an account")
                    .padding(.top, 80)
                VStack(spacing: 20) {
                    // Email field
                    EmailField(email: $registerViewModel.email)
                        .padding(.horizontal, 5)
                    // Password field
                    PasswordField(password: $registerViewModel.password)
                        .padding(.horizontal, 5)
                        .padding(.top, 16)
                    // Room number
                    RoomNumberField(roomNumber: $registerViewModel.roomNumber)
                        .padding(.horizontal, 5)
                        .padding(.top, 16)
                    // Register
                    register()
                }
                .padding()
                .onTapGesture {
                    hideKeyboard()
                }
                .alert(isPresented: $registerViewModel.errorOccurred, content: {
                    displayAlert()
                })
            }
            if registerViewModel.loading {
                RegisteringView()
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: { presentation.wrappedValue.dismiss() }) {
              Image(systemName: "arrow.backward")
                .foregroundColor(Color.electricBlue)
                .imageScale(.large)
            })
    }
}

struct RegisterTypeView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterProfileView()
    }
}
