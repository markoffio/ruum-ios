//
//  RegisterViewModel.swift
//  Ruum
//
//  Created by Marco Margarucci on 12/04/21.
//

import Foundation

class RegisterViewModel: ObservableObject {
    // Email address
    @Published var email: String = ""
    // Password
    @Published var password: String = ""
    // Room number
    @Published var roomNumber: String = ""
    // Error occurred
    @Published var errorOccurred: Bool = false
    // Register error message
    @Published var registerError: String = ""
    // Registration success
    @Published var registrationSuccess: Bool = false
    // Loading
    @Published var loading: Bool = false
    // User validator
    private let userValidator = UserValidator()
    // Authentication service
    private let authService = AuthService()
    
    // Register
    func register() {
        let user = User(email: email, password: password, roomNumber: roomNumber)
        if !userValidator.isValid(user: user) {
            errorOccurred = true
            registerError = "Non hai compilato tutti i campi richiesti"
            return
        }
        loading = true
        authService.registerUser(user: user) { [self] (error) in
            guard error == nil else {
                // Registration error
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    loading = false
                    registrationSuccess = false
                    errorOccurred = true
                    registerError = error!.localizedDescription
                }
                return
            }
            // Registration success
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                loading = false
                registrationSuccess = true
            }
        }
    }
}
