//
//  FailuresViewModel.swift
//  Ruum
//
//  Created by Marco Margarucci on 24/04/21.
//

import Foundation

class FailuresViewModel: ObservableObject {
    // Failure type
    @Published var failureType: String = ""
    // Room number
    @Published var roomNumber: String = ""
    // Report date
    @Published var date = Date()
    // User id
    @Published var userId: String = ""
    // Detected failures
    @Published var detectedFailures: [String] = []
    // Done
    @Published var done: Bool = false
    // Create report detail success
    @Published var createReportDetailSuccess: Bool = false
    // Error occurred
    @Published var errorOccurred: Bool = false
    // Create report details error
    @Published var createReportDetailsError: String = ""
    // Loading
    @Published var loading: Bool = false
    // Report service
    private let reportService = ReportService()
    // Report details validator
    private let reportDetailsValidator = ReportDetailsValidator()
    
    // Send password reset
    func setReportDetail(reportDetail: ReportDetail) {
        // Check if the user has selected almost one failure from the list
        if !reportDetailsValidator.isValid(detectedFailures: reportDetail.detectedFailures) {
            errorOccurred = true
            createReportDetailsError = "Devi selezionare almeno un guasto prima di inviare una segnalazione."
            return
        }
        loading = true
        reportService.createFailureReport(reportDetail: reportDetail) { [self] (error) in
            guard error == nil else {
                // Create report details error
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    createReportDetailSuccess = false
                    errorOccurred = true
                    createReportDetailsError = error!.localizedDescription
                }
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                loading = false
                createReportDetailSuccess = true
            }
        }
    }
}
