//
//  FailuresListView.swift
//  Ruum
//
//  Created by Marco Margarucci on 10/04/21.
//

import SwiftUI

let database: [String: [String]] = [
    "Elettrico": ["Lampadina comodino", "Interruttore principale", "Aria condizionata"],
    "Idraulico": ["Lavandino", "Doccia", "Bidet", "Perdita acqua"],
    "Camera": ["Cuscini mancanti", "Letto rotto", "Armadio rotto"],
]

struct FailuresListView: View {
    @Environment(\.presentationMode) var presentation
    // Failure detected
    @State private var detected: Bool = false
    // Report view model
    @StateObject private var reportViewModel = FailuresViewModel()
    // Current user
    let currentUser = AuthService().currentUser()
    // Failures
    let failures: [String: [String]] = database
    // Authentication service
    private let authService = AuthService()
    // Detected failures
    @State private var detectedFailures = [String: Bool]()
    
    @State private var selected: Bool = false
    
    // Display alert
    private func displayAlert() -> Alert {
        Alert(title: Text("Errore"), message: Text(reportViewModel.createReportDetailsError), dismissButton: .default(Text("OK"), action: {
            //presentationMode.wrappedValue.dismiss()
        }))
    }
    
    // Report failure
    fileprivate func reportFailure() -> some View {
        Button(action: {
            guard let user = authService.currentUser() else { return }
            let reportDetail = ReportDetail(localId: UUID().uuidString, userId: user.userId, roomNumber: user.roomNumber, date: Date(), detectedFailures: self.detectedFailures, done: false)
            reportViewModel.setReportDetail(reportDetail: reportDetail)
            self.detectedFailures.removeAll()
            presentation.wrappedValue.dismiss()
        }) {
            Text("INVIA SEGNALAZIONE")
                .bold()
                .textStyle(ButtonStyle())
                .padding(.top, 20)
        }
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                ScrollViewReader() { proxy in
                    VStack {
                        List { failuresList }
                        .listStyle(InsetGroupedListStyle())
                        // Report failure
                        reportFailure()
                    }
                    .alert(isPresented: $reportViewModel.errorOccurred, content: {
                        displayAlert()
                    })
                }
                if reportViewModel.loading {
                    ReportDetailsSendSuccessView()
                }
            }
            .navigationBarTitle(Text("Seleziona i guasti"), displayMode: .inline)
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: Button(action: { presentation.wrappedValue.dismiss() }) {
                  Image(systemName: "arrow.backward")
                    .foregroundColor(Color.electricBlue)
                    .imageScale(.large)
                })
        }
        .navigationBarHidden(true)
    }
        
    var failuresList: some View {
        ForEach(failures.sorted(by: { (lhs, rhs) -> Bool in
            lhs.key > rhs.key
        }), id: \.key) { failureCategory, failuresArray in
            Section (header: HeaderView(title: failureCategory)) {
                ForEach(failuresArray, id: \.self) { failureName in
                    FailureCellRow(detectedFailures: $detectedFailures, name: failureName)
                }
            }
        }
    }
    
    func sectionIndexTitles(proxy: ScrollViewProxy) -> some View {
        SectionIndexTitles(proxy: proxy, titles: failures.keys.sorted())
            .frame(maxWidth: .infinity, alignment: .trailing)
            .padding()
    }
}

struct HeaderView: View {
    let title: String
    
    var body: some View {
        Text(title)
            .foregroundColor(Color.text)
            .font(.title3)
            .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
            .padding(.top, 30)
            .padding(.bottom, 10)
            .frame(maxWidth: .infinity, alignment: .leading)
    }
}

struct RowView: View {
    let text: String
    
    var body: some View {
        Text(text)
            .padding()
            .frame(maxWidth: .infinity, alignment: .leading)
    }
}

struct SectionIndexTitles: View {
    let proxy: ScrollViewProxy
    let titles: [String]
    
    var body: some View {
        VStack {
            ForEach(titles, id: \.self) { title in
                Button {
                    proxy.scrollTo(title)
                } label: {
                    SectionIndexTitle(image: sfSymbol(for: title))
                }
            }
        }
    }
    
    func sfSymbol(for failureCategory: String) -> Image {
        let systemName: String
        switch failureCategory {
        case "Elettrico": systemName = "lightbulb.fill"
        case "Idraulico": systemName = "drop.fill"
        case "Camera": systemName = "bed.double.fill"
        case "Altro": systemName = "exclamationmark.triangle.fill"
        default: systemName = "xmark"
        }
        return Image(systemName: systemName)
    }
}

struct SectionIndexTitle: View {
    let image: Image
    
    var body: some View {
        RoundedRectangle(cornerRadius: 8, style: .continuous)
            .foregroundColor(Color.gray.opacity(0.1))
            .frame(width: 40, height: 40)
            .overlay(image.foregroundColor(Color.electricBlue))
    }
}


struct ReportListView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            FailuresListView()
        }
    }
}
