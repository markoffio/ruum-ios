//
//  DashboardView.swift
//  Ruum
//
//  Created by Marco Margarucci on 24/04/21.
//

import SwiftUI

struct DashboardView: View {
    
    let gridColumns = [
            GridItem(.flexible()),
            GridItem(.flexible())
        ]
    
    var body: some View {
        NavigationView {
                    VStack {
                        ScreenTitle("Welcome")
                        LazyVGrid(columns: gridColumns, alignment: .center, spacing: 24) {
                            NavigationLink(destination: ReportListingView()) {
                                ReportTypeCard(image: "bubble.left", text: "Reported")
                            }
                            // Load Report list view
                            NavigationLink(destination: FailuresListView()) {
                                ReportTypeCard(image: "wrench.and.screwdriver", text: "Failures")
                            }
                            ReportTypeCard(image: "info.circle", text: "Info")
                            ReportTypeCard(image: "gearshape", text: "Settings")
                        }
                        .frame(maxWidth: .infinity)
                        .padding()
                        Spacer()
                    }
                    .frame(maxHeight: .infinity)
                }
                .navigationBarHidden(true)
                .navigationBarBackButtonHidden(true)
    }
}


struct DashboardView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardView()
    }
}

// Report type card
fileprivate struct ReportTypeCard: View {
    // Image name
    let image: String
    // Card text
    let text: String
    
    var body: some View {
        ZStack() {
            Rectangle()
                .fill(Color.rectangleBackground)
                .frame(width: 150, height: 150)
                .cornerRadius(8.0)
                .shadow(color: Color.lightGray, radius: 20.0, x: 2, y: 1)
            Image(systemName: image)
                .resizable()
                .foregroundColor(Color.text)
                .frame(width: 60, height: 60)
                .offset(y: -20)
            Text(text)
                .foregroundColor(Color.text)
                .bold()
                .offset(y: 45)
        }
    }
}

