//
//  PasswordResetViewModel.swift
//  Ruum
//
//  Created by Marco Margarucci on 12/04/21.
//

import Foundation

class PasswordResetViewModel: ObservableObject {
    // Email
    @Published var email: String = ""
    // Show alert
    @Published var showAlert: Bool = false
    
    private let authService = AuthService()
    
    // Send password reset
    func sendPasswordReset() {
        authService.sendPasswordReset(email: email) { [self] in
            showAlert = true
        }
    }
}
