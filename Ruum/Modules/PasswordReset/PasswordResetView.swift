//
//  PasswordResetView.swift
//  Ruum
//
//  Created by Marco Margarucci on 11/04/21.
//

import SwiftUI

struct PasswordResetView: View {
    // Presentation mode
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    // State object password reset view model
    @StateObject private var passwordResetViewModel = PasswordResetViewModel()
    
    // Display alert
    private func displayAlert() -> Alert {
        Alert(title: Text("Richiesta effettuata"), message: Text("Controlla la tua casella di posta elettronica"), dismissButton: .default(Text("OK"), action: {
            presentationMode.wrappedValue.dismiss()
        }))
    }
    
    var body: some View {
        VStack(spacing: 20) {
            ScreenTitle("Recupera password")
            Text("Inserisci il tuo indirizzo email per recuperare la password")
                .fontWeight(.semibold)
                .foregroundColor(Color.text)
                .padding(.horizontal, 20)
            // Email field
            EmailField(email: $passwordResetViewModel.email)
                .padding(20)
            // Button
            Button(action: {
                // Send password reset
                passwordResetViewModel.sendPasswordReset()
            }, label: {
                Text("INVIA")
                    .bold()
                    .textStyle(ButtonStyle())
            })
            .padding(.horizontal, 10)
            Spacer()
        }
        .alert(isPresented: $passwordResetViewModel.showAlert, content: {
            displayAlert()
        })
    }
}

struct PasswordResetView_Previews: PreviewProvider {
    static var previews: some View {
        PasswordResetView()
    }
}
