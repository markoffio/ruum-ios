//
//  OnboardingView.swift
//  Ruum
//
//  Created by Marco Margarucci on 05/04/21.
//

import SwiftUI

struct OnboardingView: View {
    // Show login
    @State private var showLogin: Bool = false
    
    var onboardingData: [OnboardingItem] = [
        OnboardingItem(imageName: "receptionist", title: "ruum", description: "Comunica alla reception eventuali malfunzionamenti riscontrati nella tua camera."),
        OnboardingItem(imageName: "assistant", title: "Comunicazione immediata", description: "La tua segnalazione arriva direttamente alla reception."),
        OnboardingItem(imageName: "enjoy", title: "Riparazione veloce", description: "Un addetto provvederà alla riparazione del guasto da te segnalato.")
    ]
    
    var body: some View {
        VStack {
            // TabView
            TabView {
                ForEach(0..<onboardingData.count) { index in
                    let currentElement = onboardingData[index]
                    OnboardingCard(onboardingItem: currentElement)
                }
            }
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .automatic))
            .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
            // Login Button
            Button(action: {
                showLogin.toggle()
            }, label: {
                Text("LOGIN")
                    .bold()
                    .textStyle(ButtonStyle())
            })
            .padding(.top, 10)
            .padding(.bottom, 35)
        }
        .onAppear {
            UserDefaults.standard.setValue(true, forKey: Constants.REGISTERED)
        }
        .fullScreenCover(isPresented: $showLogin, content: {
            LoginView()
        })
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView()
    }
}

// Onboarding card
fileprivate struct OnboardingCard: View {
    // Onboarding item
    let onboardingItem: OnboardingItem
    
    var body: some View {
        GeometryReader { geometry in
            VStack(alignment: .center) {
                Image(onboardingItem.imageName)
                    .resizable()
                    .frame(height: geometry.size.height / 2 )
                    .frame(maxWidth: .infinity)
                Text(onboardingItem.title)
                    .font(.title)
                    .foregroundColor(Color.text)
                    .bold()
                    .padding()
                Text(onboardingItem.description)
                    .multilineTextAlignment(.center)
                    .font(.body)
                    .foregroundColor(.gray)
                    .padding(.horizontal, 15)
            }
        }
    }
}
