//
//  ReportListingViewModel.swift
//  Ruum
//
//  Created by Marco Margarucci on 18/04/21.
//

import Foundation

class ReportListingViewModel: ObservableObject {
    @Published var reports: [ReportDetail] = []
    
    private var reportedFailureService = ReportedFailureService()
    
    init() {
        reportedFailureService.getReportedFailures { [self] (fetchedReportedFailures) in
            DispatchQueue.main.async {
                if let fetchedReportedFailures = fetchedReportedFailures {
                    reports = fetchedReportedFailures
                }
            }
        }
    }
}
