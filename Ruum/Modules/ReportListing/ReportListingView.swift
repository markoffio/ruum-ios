//
//  ReportListingView.swift
//  Ruum
//
//  Created by Marco Margarucci on 18/04/21.
//

import SwiftUI

struct ReportListingView: View {
    @StateObject var reportListingViewModel = ReportListingViewModel()
    @Environment(\.presentationMode) var presentation
    
    var body: some View {
        NavigationView {
            ZStack {
                ScrollView(.vertical, showsIndicators: false) {
                    LazyVStack {
                        ForEach(reportListingViewModel.reports, id: \.localId) {
                            item in
                            ReportedFailureCellRow(reportDetail: item)
                                .padding(.leading, 20)
                                .padding(.trailing, 20)
                        }
                    }
                }
            }
            .navigationBarTitle(Text("Your reports"), displayMode: .inline)
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: Button(action: { presentation.wrappedValue.dismiss() }) {
                  Image(systemName: "arrow.backward")
                    .foregroundColor(Color.electricBlue)
                    .imageScale(.large)
                })
        }
        .navigationBarHidden(true)
    }
}

struct ReportListingView_Previews: PreviewProvider {
    static var previews: some View {
        ReportListingView()
    }
}
