//
//  Constants.swift
//  Ruum
//
//  Created by Marco Margarucci on 17/04/21.
//

import Foundation

struct Constants {
    // Firebase collections
    // Users collection
    static let USERS_COLLECTION: String = "users"
    // Rooms collection
    static let ROOMS_COLLECTION: String = "rooms"
    // Reports collection
    static let REPORTS_COLLECTION: String = "reports"
    // Reports solved
    static let REPORT_SENT: String = "sent"
    
    // Flags
    // User logged in
    static let LOGGED_IN: String = "loggedIn"
    // Already registered
    static let REGISTERED: String = "registered"
    // Current user
    static let CURRENT_USER: String = "currentUser"
    // Failure already signaled
    static let SIGNALED: String = "signaled"
    
    // Failure type
    // Electric
    static let ELECTRIC: String = "electric"
    // Hydraulic
    static let HYDRAULIC: String = "hydraulic"
    // Room
    static let ROOM: String = "room"
    // Other
    static let OTHER: String = "other"
    
    
    
}
