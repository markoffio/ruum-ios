//
//  RuumApp.swift
//  Ruum
//
//  Created by Marco Margarucci on 05/04/21.
//

import SwiftUI
import Firebase

@main
struct RuumApp: App {
    
    init() {
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            // Check if the user is already logged in
            if UserDefaults.standard.bool(forKey: Constants.LOGGED_IN) {
                // Show report view
                //ReportListView()
                DashboardView()
            } else if UserDefaults.standard.bool(forKey: Constants.REGISTERED) { // If the user is already registered
                // Show login view
                LoginView()
            } else {
                // Show onboarding view
                OnboardingView()
            }
        }
    }
}
