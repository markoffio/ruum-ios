//
//  AuthService.swift
//  Ruum
//
//  Created by Marco Margarucci on 12/04/21.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

struct AuthService {
    
    // Database reference
    private var db = Firestore.firestore()
    
    // Get current user
    func currentUser() -> User? {
        guard Auth.auth().currentUser != nil else { return nil }
        if let savedUser = UserDefaults.standard.object(forKey: Constants.CURRENT_USER) as? Data {
            let decoder = JSONDecoder()
            if let loadedUser = try? decoder.decode(User.self, from: savedUser) {
                return loadedUser
            }
        }
        return nil
    }
    
    // Login
    func loginUser(withEmail email: String, andPassword password: String, completion: @escaping(_ error: Error?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            // Check for errors
            guard error == nil else {
                completion(error)
                return
            }
            guard let authResult = authResult else { return }
            self.saveUserToDefaults(userID: authResult.user.uid)
            completion(nil)
        }
    }
    
    // Send password reset instructions
    func sendPasswordReset(email: String, completion: @escaping() -> Void) {
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            completion()
        }
    }
    
    // Register
    func registerUser(user: User, completion: @escaping(_ error: Error?) -> Void) {
        // Create user
        Auth.auth().createUser(withEmail: user.email, password: user.password) { (authResult, error) in
            // Check for errors
            guard error == nil else {
                completion(error)
                return
            }
            guard let authResult = authResult else { return }
            user.userId = authResult.user.uid
            do {
                // Add the user entry to users collection
                let _ = try db.collection(Constants.USERS_COLLECTION).addDocument(from: user)
                completion(nil)
            } catch let registerError {
                completion(registerError)
            }
        }
    }
    
    // Save user to default
    private func saveUserToDefaults(userID: String) {
        db.collection(Constants.USERS_COLLECTION)
            .whereField("userId", isEqualTo: userID)
            .getDocuments { (querySnapshot, error) in
                if error != nil { return }
                guard let documents = querySnapshot?.documents else { return }
                if let user = try? documents.first?.data(as: User.self) {
                    UserDefaults.standard.set(true, forKey: Constants.LOGGED_IN)
                    if let encoded = try? JSONEncoder().encode(user) {
                        UserDefaults.standard.set(encoded, forKey: Constants.CURRENT_USER)
                    }
                }
            }
    }
}
