//
//  ReportService.swift
//  Ruum
//
//  Created by Marco Margarucci on 24/04/21.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

struct ReportService {
    
    // Database reference
    private var db = Firestore.firestore()
    
    // Create failure entry
    func createFailureReport(reportDetail: ReportDetail, completion: @escaping(_ error: Error?) -> Void) {
        // Set document data
        db.collection(Constants.REPORTS_COLLECTION).document(reportDetail.roomNumber).setData(["userId" : reportDetail.userId])
        do {
            // Add report collections
            let _ = try db.collection(Constants.REPORTS_COLLECTION).document(reportDetail.roomNumber).collection(Constants.REPORT_SENT).addDocument(from: reportDetail)
            completion(nil)
        } catch let createFailureEntryError {
            completion(createFailureEntryError)
        }
    }
}
