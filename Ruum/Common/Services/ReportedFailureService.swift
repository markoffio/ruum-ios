//
//  ReportedFailureService.swift
//  Ruum
//
//  Created by Marco Margarucci on 25/04/21.
//

import Foundation
import Firebase
import FirebaseFirestoreTarget

struct ReportedFailureService {
    
    // Database reference
    private var db = Firestore.firestore()
    
    // Authentication service
    private let authService = AuthService()
    
    // Get all reported failure
    func getReportedFailures(completion: @escaping ([ReportDetail]?) -> Void) {
        guard let user = authService.currentUser() else { return }
        db.collection(Constants.REPORTS_COLLECTION).document(user.roomNumber).collection(Constants.REPORT_SENT).getDocuments { (querySnapshot, error) in
            if error != nil {
                completion(nil)
                return
            }
            
            guard let documents = querySnapshot?.documents else {
                completion(nil)
                return
            }
            
            var fetchedReportedFailures: [ReportDetail] = []
            
            for document in documents {
                if let reportDetail = try? document.data(as: ReportDetail.self) {
                    fetchedReportedFailures.append(reportDetail)
                }
            }
            completion(fetchedReportedFailures)
        }
    }
}
