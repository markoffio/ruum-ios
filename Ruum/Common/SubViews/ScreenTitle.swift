//
//  ScreenTitle.swift
//  Ruum
//
//  Created by Marco Margarucci on 11/04/21.
//

import SwiftUI

// Screen title
struct ScreenTitle: View {
    let title: String
    
    init (_ title: String) {
        self.title = title
    }
    
    var body: some View {
        HStack {
            Text(title)
                .font(.largeTitle)
                .bold()
                .foregroundColor(Color.text)
                .padding(.bottom, 30)
                .padding(.leading, 20)
            Spacer()
        }
    }
}

struct ScreenTitle_Previews: PreviewProvider {
    static var previews: some View {
        ScreenTitle("Hello world")
    }
}
