//
//  FailureCellRow.swift
//  Ruum
//
//  Created by Marco Margarucci on 11/04/21.
//

import SwiftUI

struct FailureCellRow: View {
    // Failure selected
    @State private var selected: Bool = false
    //@Binding var selected: Bool
    // Failure detected
    @Binding var detectedFailures: [String: Bool]
    // Failure name
    let name: String
    
    var body: some View {
        HStack() {
            // Text
            Text(name)
                .foregroundColor(Color.text)
                .fontWeight(.semibold)
                .padding(.leading, 10)
                .padding(.top, 20)
                .padding(.bottom, 20)
            Spacer()
            // Circle image
            Image(systemName: selected ? "circle.fill": "circle")
                .resizable()
                .frame(width: 18.0, height: 18.0)
                .foregroundColor(Color.electricBlue)
                .onTapGesture {
                    selected.toggle()
                    if selected { self.detectedFailures.updateValue(false, forKey: name) }
                    else { self.detectedFailures.removeValue(forKey: name) } //= self.detectedFailures.filter { $0 != [name: false] } }
                    dump(self.detectedFailures)
                }
        }
    }
}

/*
struct FailureCellRow_Previews: PreviewProvider {
    var detected: Bool
    static var previews: some View {
        FailureCellRow(detected: false, type: "Elettrico", name: "Interruttore stanza")
    }
}
*/
