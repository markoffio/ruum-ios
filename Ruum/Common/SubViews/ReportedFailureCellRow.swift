//
//  ReportedFailureCellRow.swift
//  Ruum
//
//  Created by Marco Margarucci on 25/04/21.
//

import SwiftUI

struct ReportedFailureCellRow: View {
    let reportDetail: ReportDetail
    
    var body: some View {
        HStack() {
            Text(reportDetail.date.toString(format: "d MMMM y  -  HH:mm"))
                .foregroundColor(Color.text)
                .fontWeight(.semibold)
                //.padding(.leading, 10)
                .padding(.top, 20)
                .padding(.bottom, 20)
            Spacer()
            // Circle image
            Image(systemName: "circle.fill")
                .resizable()
                .frame(width: 18.0, height: 18.0)
                .foregroundColor(reportDetail.done ? Color.deepGreen : Color.deepRed)
        }
    }
}
/*
struct ReportedFailureCellRow_Previews: PreviewProvider {
    static var previews: some View {
        ReportedFailureCellRow(reportDetail: reportDetailData[0])
    }
}
*/
