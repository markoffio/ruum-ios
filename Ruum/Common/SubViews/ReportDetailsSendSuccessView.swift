//
//  ReportDetailsSendSuccessView.swift
//  Ruum
//
//  Created by Marco Margarucci on 25/04/21.
//

import SwiftUI

struct ReportDetailsSendSuccessView: View {
    @State private var show = false
    
    var body: some View {
        VStack(alignment: .center, spacing: 10) {
            Text("Segnalazione inviata con successo")
                .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                .foregroundColor(Color.text)
                .bold()
                .padding()
                .opacity(show ? 1 : 0)
                .animation(Animation.linear(duration: 1).delay(0.2))
            LottieView(animationName: "done")
                .frame(width: 100, height: 100)
                .opacity(show ? 1 : 0)
                .animation(Animation.linear(duration: 1).delay(0.3))
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .center)
        .background(Blur(style: .systemUltraThinMaterial).ignoresSafeArea(.all))
        .onAppear {
            show.toggle()
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
    }
}

struct ReportDetailsSendSuccessView_Previews: PreviewProvider {
    static var previews: some View {
        ReportDetailsSendSuccessView()
    }
}

