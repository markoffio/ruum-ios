//
//  BlinkView.swift
//  Ruum
//
//  Created by Marco Margarucci on 25/04/21.
//

import SwiftUI

struct BlinkView: View {
    @State private var show = true
    
    var body: some View {
        LottieView(animationName: "pulse")
            .frame(width: 35, height: 35)
            .opacity(show ? 1 : 0)
            .animation(Animation.linear(duration: 1).delay(0.3))
            .opacity(0.7)
    }
}

struct BlinkView_Previews: PreviewProvider {
    static var previews: some View {
        BlinkView()
    }
}
