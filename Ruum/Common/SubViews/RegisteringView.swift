//
//  RegisteringView.swift
//  Ruum
//
//  Created by Marco Margarucci on 17/04/21.
//

import SwiftUI

struct RegisteringView: View {
    @State private var show = false
    
    var body: some View {
        VStack(alignment: .center, spacing: 10) {
            Text("Registrazione in corso")
                .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                .foregroundColor(Color.text)
                .bold()
                .padding()
                .opacity(show ? 1 : 0)
                .animation(Animation.linear(duration: 1).delay(0.2))
            LottieView(animationName: "loader")
                .frame(width: 100, height: 100)
                .opacity(show ? 1 : 0)
                .animation(Animation.linear(duration: 1).delay(0.3))
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .center)
        .background(Blur(style: .systemUltraThinMaterial).ignoresSafeArea(.all))
        .onAppear {
            show.toggle()
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        RegisteringView()
    }
}
