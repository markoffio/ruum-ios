//
//  ReportDetailsValidator.swift
//  Ruum
//
//  Created by Marco Margarucci on 24/04/21.
//

import Foundation

struct ReportDetailsValidator {
    
    func isValid(detectedFailures: [String: Bool]) -> Bool {
        // Check if the array is empty
        guard !detectedFailures.isEmpty else { return false }
        return true
    }
}
