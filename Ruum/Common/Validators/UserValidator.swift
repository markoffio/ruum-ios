//
//  UserValidator.swift
//  Ruum
//
//  Created by Marco Margarucci on 12/04/21.
//

import Foundation

struct UserValidator {
    
    func isValid(user: User) -> Bool {
        // Check if email field is empty
        guard !user.email.isEmpty else { return false }
        // Check if the password field is empty
        guard !user.password.isEmpty else { return false }
        // Check if room number field is empty
        guard !user.roomNumber.isEmpty else { return false }
        return true
    }
}
