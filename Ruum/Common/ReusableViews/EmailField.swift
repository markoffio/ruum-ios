//
//  EmailField.swift
//  Ruum
//
//  Created by Marco Margarucci on 11/04/21.
//

import SwiftUI

struct EmailField: View {
    @Binding var email: String
    
    var body: some View {
        VStack {
            HStack {
                TextField("email", text: $email, onCommit: {
                    hideKeyboard()
                })
                .keyboardType(.emailAddress)
                .autocapitalization(.none)
                .disableAutocorrection(true)
                Image(systemName: "envelope")
                    .foregroundColor(Color.gray.opacity(0.5))
            }
            //.font(.title3)
            .padding(.bottom, 2)
            .padding(.horizontal, 8)
            Divider()
                .frame(height: 0.0)
                .padding(.horizontal, 10)
                .background(Color.gray)
        }
        //.overlay(RoundedRectangle(cornerRadius: 5.0).stroke(Color.lightGray, lineWidth: 1))
    }
}

struct EmailField_Previews: PreviewProvider {
    static var previews: some View {
        EmailField(email: .constant("margarucci.marco@gmail.com"))
    }
}
