//
//  PasswordField.swift
//  Ruum
//
//  Created by Marco Margarucci on 11/04/21.
//

import SwiftUI

struct PasswordField: View {
    @Binding var password: String

    var body: some View {
        VStack {
            HStack {
                SecureField("password", text: $password, onCommit: {
                    hideKeyboard()
                })
                .padding(.leading, 2)
                Image(systemName: "key")
                    .foregroundColor(Color.gray.opacity(0.5))
                    .padding(.trailing, 4)
            }
            //.font(.title3)
            .padding(.bottom, 2)
            .padding(.horizontal, 8)
            Divider()
                .frame(height: 0.0)
                .padding(.horizontal, 10)
                .background(Color.gray)
            //.overlay(RoundedRectangle(cornerRadius: 5.0).stroke(Color.lightGray, lineWidth: 1))
        }
    }
}

struct PasswordField_Previews: PreviewProvider {
    static var previews: some View {
        PasswordField(password: .constant(""))
    }
}
