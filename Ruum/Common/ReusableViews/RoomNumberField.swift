//
//  RoomNumberField.swift
//  Ruum
//
//  Created by Marco Margarucci on 12/04/21.
//

import SwiftUI

struct RoomNumberField: View {
    @Binding var roomNumber: String
    
    var body: some View {
        VStack {
            HStack {
                TextField("please enter room number", text: $roomNumber, onCommit: {
                    hideKeyboard()
                })
                .keyboardType(.numberPad)
                .autocapitalization(.none)
                .disableAutocorrection(true)
                Image(systemName: "3.square")
                    .foregroundColor(Color.gray.opacity(0.5))
                    .padding(.trailing, 2)
            }
            //.font(.title3)
            .padding(.bottom, 2)
            .padding(.horizontal, 8)
            Divider()
                .frame(height: 0.0)
                .padding(.horizontal, 10)
                .background(Color.gray)
        }
        //.overlay(RoundedRectangle(cornerRadius: 5.0).stroke(Color.lightGray, lineWidth: 1))
    }
}

struct RoomNumberField_Previews: PreviewProvider {
    static var previews: some View {
        RoomNumberField(roomNumber: .constant("89"))
    }
}
