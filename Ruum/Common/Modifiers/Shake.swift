//
//  Shake.swift
//  Ruum
//
//  Created by Marco Margarucci on 25/04/21.
//

import SwiftUI

struct Shake: GeometryEffect {
    var amount: CGFloat = 10
    var shakesPerUnit = 3
    var animatableData: CGFloat

    func effectValue(size: CGSize) -> ProjectionTransform {
        ProjectionTransform(CGAffineTransform(translationX: amount * sin(animatableData * .pi * CGFloat(shakesPerUnit)), y: 0))
    }
}

/*
struct Shake_Previews: PreviewProvider {
    static var previews: some View {
        Shake()
    }
}
*/
