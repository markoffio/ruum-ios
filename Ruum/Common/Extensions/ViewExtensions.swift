//
//  ViewExtensions.swift
//  Ruum
//
//  Created by Marco Margarucci on 12/04/21.
//

import Foundation
import SwiftUI

extension View {
    // Hide keyboard
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
