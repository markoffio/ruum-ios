//
//  ColorExtensions.swift
//  Ruum
//
//  Created by Marco Margarucci on 05/04/21.
//

import Foundation
import SwiftUI

extension Color {
    // Electric blue
    static let electricBlue = Color("electric-blue")
    // Deep blue
    static let deepBlue = Color("deep-blue")
    // Deep red
    static let deepRed = Color("deep-red")
    // Deep green
    static let deepGreen = Color("deep-green")
    // Light gray
    static let lightGray = Color("light-gray")
    // Rectangle background
    static let rectangleBackground = Color("rectangle-background")
    // Text
    static let text = Color("text")
}
