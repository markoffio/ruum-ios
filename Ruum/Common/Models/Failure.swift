//
//  Failure.swift
//  Ruum
//
//  Created by Marco Margarucci on 11/04/21.
//

import Foundation
import FirebaseFirestoreSwift

struct Failure: Identifiable, Codable {
    @DocumentID var id: String?
    
    // Local identifier
    let localId: String
    // Failure type
    let type: String
    // Failure name
    let name: String
    // Failure detected
    let detected: Bool
}

let failureData = [
    Failure(localId: UUID().uuidString, type: "Idraulico", name: "Perdita acqua", detected: false),
    Failure(localId: UUID().uuidString, type: "Elettrico", name: "Aria condizionata rotta", detected: false),
    Failure(localId: UUID().uuidString, type: "Elettrico", name: "Lampadina stanza rotta", detected: false),
    Failure(localId: UUID().uuidString, type: "Camera", name: "Manca cuscino", detected: false),
    Failure(localId: UUID().uuidString, type: "Idraulico", name: "Acqua calda doccia", detected: false)
]
