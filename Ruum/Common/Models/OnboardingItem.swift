//
//  OnboardingItem.swift
//  Ruum
//
//  Created by Marco Margarucci on 05/04/21.
//

import Foundation

struct OnboardingItem {
    let imageName: String
    let title: String
    let description: String
}
