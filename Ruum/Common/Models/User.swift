//
//  User.swift
//  Ruum
//
//  Created by Marco Margarucci on 12/04/21.
//

import Foundation
import FirebaseFirestoreSwift
import FirebaseFirestore

class User: Codable, Identifiable {
    // Document id used to store a user in Firestore
    @DocumentID var id: String?
    // User id
    var userId: String = ""
    // Email address
    let email: String
    // Password
    var password: String = ""
    // Room number
    let roomNumber: String
    
    init(email: String, password: String, roomNumber: String) {
        self.email = email
        self.password = password
        self.roomNumber = roomNumber
    }
    
    private enum CodingKeys: String, CodingKey {
        case email
        case roomNumber
        case userId
    }
}
