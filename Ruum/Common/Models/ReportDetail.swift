//
//  ReportDetails.swift
//  Ruum
//
//  Created by Marco Margarucci on 10/04/21.
//

import Foundation
import FirebaseFirestoreSwift
import FirebaseFirestore

struct ReportDetail: Identifiable, Codable {
    // Document id used to store a report detail in Firestore
    @DocumentID var id: String?
    // Local id
    let localId: String
    // User id
    var userId: String// = ""
    // Room number
    let roomNumber: String
    // Date
    var date: Date// = Date()
    // Detected failures
    var detectedFailures: [String: Bool]//()
    // Done
    var done: Bool
    /*
    init(userId: String, roomNumber: String, date: Date, detectedFailures: [String: Bool], done: Bool) {
        self.userId = userId
        self.roomNumber = roomNumber
        self.date = date
        self.detectedFailures = detectedFailures
        self.done = done
    }
    
    private enum CodingKeys: String, CodingKey {
        case userId
        case date
        case detectedFailures
        case roomNumber
        case done
    }
     */
}
